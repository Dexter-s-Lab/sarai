<?php

/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //


/**
*  The name of the database for WordPress
*/
define('DB_NAME', 'c8bitlab_sarai');

/**
*  MySQL database username
*/
define('DB_USER', 'c8bitlab_dex');

/**
*  MySQL database username
*/
define('DB_PASSWORD', 'LplF1C5#,$RO');

/**
*  MySQL hostname
*/
define('DB_HOST', 'localhost');

/**
*  Database Charset to use in creating database tables.
*/
define('DB_CHARSET', 'utf8');

/**
*  The Database Collate type. Don't change this if in doubt.
*/
define('DB_COLLATE', '');

/**
*  WordPress Database Table prefix.
*  You can have multiple installations in one database if you give each a unique
*  prefix. Only numbers, letters, and underscores please!
*/
$table_prefix = 'wp_';

/**
*  disallow unfiltered HTML for everyone, including administrators and super administrators. To disallow unfiltered HTML for all users, you can add this to wp-config.php:
*/
define('DISALLOW_UNFILTERED_HTML', false);

/**
*  
*/
define('ALLOW_UNFILTERED_UPLOADS', false);

/**
*  The easiest way to manipulate core updates is with the WP_AUTO_UPDATE_CORE constant
*/
define('WP_AUTO_UPDATE_CORE', true);

/**
*  forces the filesystem method
*/
define('FS_METHOD', 'direct');

/**
*  Authentication Unique Keys and Salts.
*  Change these to different unique phrases!
*  You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
*  You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
*  @since 2.6.0
*/
define('AUTH_KEY', 'Z`_O >JD$v4^eFi}Y^nUlTJec[)#?fXPF9?3qi+-QYjPJF_lP*s5Fi P`G%U1T,=');
define('SECURE_AUTH_KEY', 'o0BxrkV^c;]3i&Q(3(^G^/0NRU`V!>NU]T059qOOS#TLGg%E-N7eqdxcg~zIc_{R');
define('LOGGED_IN_KEY', '|DRqSWK>=ak}+?|D3q}9O[UMbK~1}ds+}21MKYnBh$09t_/!%P>X>8S@7?/l9@Q:');
define('NONCE_KEY', 'u[J(K!!uW~WcqAz,WL4j,~orq3eGk-X&=1KSY6f>t(!,Sgzax.&/*{ShhZ AR|Qp');
define('AUTH_SALT', '2tW^Gw(+#4q4y6]2G(Jw?kd*BEA^TiVv/l 1q$IQqQl2klaz[@m,$6+EdzJ(].+j');
define('SECURE_AUTH_SALT', 'H:36Dkz{+V)cq)07i1Cgr+)T<g0ho>sHF%(#20?10=5Xd/u|k++7GuYQ1.ae40Qi');
define('LOGGED_IN_SALT', 'y-E]t$AT{~@NuJyJRJ8,lvhl+ZniYj0cP>!9Z5?^Lt`,3h^f{eA?%9fpZ.n}k-S/');
define('NONCE_SALT', 'K^h6t(-VQbZoul4FmPy]2m8j6Wcc%1q*ugk/a3Hhf|(K95Rgrb+p{UK_1lsoSv03');

/**
*  For developers: WordPress debugging mode.
*  Change this to true to enable the display of notices during development.
*  It is strongly recommended that plugin and theme developers use WP_DEBUG
*  in their development environments.
*/
define('WP_DEBUG', false);
define('WP_DEBUG_LOG', false);
define('WP_DEBUG_DISPLAY', true);

/**
*  For developers: WordPress Script Debugging
*  Force Wordpress to use unminified JavaScript files
*/
define('SCRIPT_DEBUG', false);

/**
*  WordPress Localized Language, defaults to English.
*  Change this to localize WordPress. A corresponding MO file for the chosen
*  language must be installed to wp-content/languages. For example, install
*  de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
*  language support.
*/
define('WPLANG', '');

/**
*  Setup Multi site
*/
define('WP_ALLOW_MULTISITE', false);

/**
*  Wordpress Cache!!
*/

/**
*  Post Autosave Interval
*/
define('AUTOSAVE_INTERVAL', 60);

/**
*  Disable / Enable Post Revisions and specify revisions max count
*/
define('WP_POST_REVISIONS', true);

/**
*  this constant controls the number of days before WordPress permanently deletes 
*  posts, pages, attachments, and comments, from the trash bin
*/
define('EMPTY_TRASH_DAYS', 30);

/**
*  Make sure a cron process cannot run more than once every WP_CRON_LOCK_TIMEOUT seconds
*/
define('WP_CRON_LOCK_TIMEOUT', 60);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
