<?php

/*
 Template Name: Thank You
 */

 ?>
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0">

<base href="<?php bloginfo('template_directory');?>/twentyfifteen"/>

<!-- <script type="text/javascript" src="assets/js/jquery-2.1.4.min.js"></script> -->
<script type="text/javascript" src="assets/js/jquery-2.1.3.min.js"></script>

<link href="assets/css/bootstrap.min.css" rel="stylesheet" media="screen">
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<!-- <script type="text/javascript" src="assets/fancybox-videos_2/lib/jquery-1.10.1.min.js"></script> --> 

<link href="assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" media="screen">

<script type="text/javascript" src="assets/js/bootstrap-submenu.min.js"></script>
<script type="text/javascript" src="assets/js/docs.js"></script>

<script type="text/javascript" src="assets/js/visible.js"></script>

<link href="assets/owl_carousel_assets/owl.carousel.css" rel="stylesheet" media="screen">
<link href="assets/owl_carousel_assets/owl.theme.css" rel="stylesheet" media="screen">
<link href="assets/owl_carousel_assets/owl.transitions.css" rel="stylesheet" media="screen">
<script type="text/javascript" src="assets/owl_carousel_assets/owl.carousel.js"></script>

<!-- <link href="assets/css/bootstrap-submenu.min.css" rel="stylesheet" media="screen"> -->

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '1162650763829848', {
em: 'insert_email_variable,'
});
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1162650763829848&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->

<?php wp_head() ?>
</head>

<head>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '1162650763829848'); // Insert your pixel ID here.
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1162650763829848&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
</head>
<script>fbq('track', '<EVENT_NAME>');</script>


<script type="text/javascript">
function initMap() 
{

    var mapCanvas1 = document.getElementById('map1');
    var mapOptions1 = {
      center: new google.maps.LatLng(30.101870, 31.691361),
      // center: new google.maps.LatLng(31.691361, 30.101870),
      zoom: 17,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    var map1 = new google.maps.Map(mapCanvas1, mapOptions1);
    var position_1 = {lat: 30.101870, lng: 31.691361};
    // var position_1 = {lat: 31.691361, lng: 30.101870};

    var marker_1 = new google.maps.Marker({
      position: position_1,
      map: map1,
      title: 'Sarai'
    });

}
</script>


<link href="style_home.css" rel="stylesheet" media="screen">

<style type="text/css">

.outer_container
{
  display: block;
}

.top_div_main
{
  padding-top: 30px;
  padding-bottom: 30px;
}


.top_div_text_2_span_1 
{
  width: 80%;
  margin-left: auto;
  margin-right: auto;
  text-transform: inherit;
  letter-spacing: 0;
}

@media screen and (max-width: 500px) {

.top_div_text_2_span_1 
{
  font-size: 15px;
}

.top_div_text_2
{
  padding-top: 4%;
}

}

</style>
<title><?php echo $page_title; ?></title>
<body>

<!-- Google Code for Sarai Leads Q1 Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 869534557;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "Xx8jCNidgG4Q3ZbQngM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/869534557/?label=Xx8jCNidgG4Q3ZbQngM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<img src="http://bcp.crwdcntrl.net/5/c=5995/b=35038892" width="1" height="1" class="ad_img"/>

<div class="outer_container container">
  <div class="top_div" id="home">
    <div class="top_div_main">
      <img src="assets/img/sarai_logo.png"/>
    </div>
    <div class="top_div_text_2 module" id="about_us">
      <span class="top_div_text_2_span_1">Thank you for your registeration. Our sales team will get in touch with you soon.</span>
    </div>
  </div>
  <div class="footer_div footer container">
    <div class="col-sm-3 pri_div">
      <a href="<?php echo get_permalink( 233 ); ?>">
        <span>Privacy Policy</span>
      </a>
    </div>
    <div class="col-sm-6">
      <span class="footer_div_span">All rights reserved. MNHD <?php echo date('Y')?></span>
      <div class="footer_icons">
        <a href="https://www.facebook.com/SaraiNewCairo" target="_blank">
          <img src="assets/img/fb_icon.png"/>
        </a>
        <a href="https://www.instagram.com/sarai_newcairo/" target="_blank">
          <img src="assets/img/insta_icon.png"/>
        </a>
      </div>
    </div>
    <div class="col-sm-3 footer_last">
      <span>A project by: </span>
      <span>Madinet Nasr for Housing & Development (MNHD)</span>
    </div>
  </div>
</div>
<script>
fbq('track', 'Lead', {
value: 10.00,
currency: 'USD'
});
</script>

<?php get_footer(); ?>



</body>
