<?php

 header('Access-Control-Allow-Origin: *');
    // header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
    // header('Access-Control-Allow-Headers: Origin, Content-Type, Accept, Authorization, X-Request-With');
    // header('Access-Control-Allow-Credentials: true');


/*
 Template Name: Landing Page
 */

 ?>
<?php get_header(); ?>

<script type="text/javascript">
function initMap() 
{

    var mapCanvas1 = document.getElementById('map1');
    var mapOptions1 = {
      center: new google.maps.LatLng(30.101870, 31.691361),
      // center: new google.maps.LatLng(31.691361, 30.101870),
      zoom: 12,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    var map1 = new google.maps.Map(mapCanvas1, mapOptions1);
    var position_1 = {lat: 30.101870, lng: 31.691361};
    // var position_1 = {lat: 31.691361, lng: 30.101870};

    var marker_1 = new google.maps.Marker({
      position: position_1,
      map: map1,
      title: 'Sarai',
      icon: $('.marker_url').val()
    });

}
</script>

<?php

$new_url = do_shortcode('[urlparam param="source"/]');
if(empty($new_url))
$new_url = '';

// print_r($new_url);

$video = get_field('video', get_the_ID());

$services_array = array();
$args = array( 'numberposts' => -1, 'post_type'=>'services', 'suppress_filters' => 0);
$services = get_posts( $args );
foreach ( $services as $post ) :   setup_postdata( $post );

$title = $post -> post_title;
$content = get_field('content', $post -> ID);
$image = get_field('image', $post -> ID);

$content = strip_shortcodes($content);
$content = strip_tags($content);

$excerpt = substr($content, 0, 100);
$excerpt = $excerpt.'...';

$banner = wp_get_attachment_url( get_post_thumbnail_id($post -> ID) );

$services_array[] = array('title' => $title, 'content' => $content, 'excerpt' => $excerpt, 'banner' => $banner, 'image' => $image);

endforeach;
wp_reset_postdata();

// print_r($services_array);die;


$perspectives_array = array();
$args = array( 'numberposts' => -1, 'post_type'=>'perspectives', 'suppress_filters' => 0);
$perspectives = get_posts( $args );
foreach ( $perspectives as $post ) :   setup_postdata( $post );

$thumb = get_field('thumb', $post -> ID);
$image = get_field('image', $post -> ID);

$perspectives_array[] = array('thumb' => $thumb, 'image' => $image);

endforeach;
wp_reset_postdata();


$floor_plans_array = array();
$args = array( 'numberposts' => -1, 'post_type'=>'floor_plans', 'suppress_filters' => 0);
$floor_plans = get_posts( $args );
foreach ( $floor_plans as $post ) :   setup_postdata( $post );

$thumb = get_field('thumb', $post -> ID);
$image = get_field('image', $post -> ID);

$floor_plans_array[] = array('thumb' => $thumb, 'image' => $image);


endforeach;
wp_reset_postdata();


$svilla_perspectives_array = array();
$args = array( 'numberposts' => -1, 'post_type'=>'svillapresp', 'suppress_filters' => 0);
$svilla_perspectives = get_posts( $args );
foreach ( $svilla_perspectives as $post ) :   setup_postdata( $post );

$thumb = get_field('thumb', $post -> ID);
$image = get_field('image', $post -> ID);

$svilla_perspectives_array[] = array('thumb' => $thumb, 'image' => $image);

endforeach;
wp_reset_postdata();


$svilla_floor_plans_array = array();
$args = array( 'numberposts' => -1, 'post_type'=>'svillafloor', 'suppress_filters' => 0);
$svilla_floor_plans = get_posts( $args );
foreach ( $svilla_floor_plans as $post ) :   setup_postdata( $post );

$thumb = get_field('thumb', $post -> ID);
$image = get_field('image', $post -> ID);

$svilla_floor_plans_array[] = array('thumb' => $thumb, 'image' => $image);


endforeach;
wp_reset_postdata();

// print_r($floor_plans_array);

?>
<link href="style_home_3.css?v=1.2" rel="stylesheet" media="screen">


<title><?php echo $page_title; ?></title>
<body>

<img src="http://bcp.crwdcntrl.net/5/c=5995/b=35038904" width="1" height="1" class="ad_img"/>

<!-- Google Code for SARAI Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 869534557;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "_ELECN62umsQ3ZbQngM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/869534557/?label=_ELECN62umsQ3ZbQngM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<!-- <div class="overlay_2" id="landing_ad">
<div class="landing_x_div">
  <span class="landing_ad_x">X</span>
  <span class="landing_ad_span_1">Introducing</span>
  <span class="landing_ad_span_2">The S-Villa</span>
</div>
<div class="videoWrapper">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/py_8cPSKmL8?autoplay=1&rel=0&showinfo=0&controls=0" frameborder="0" allowfullscreen></iframe>
  </div>
  <p class="landing_ad_p">Own your dream family home in a one-of-a-kind S-villa at Sarai, New Cairo, starting from 260 sqm with 0% down payment and instalment plans up to 10 years. Don't let the opportunity go to waste, sign up today!</p>
</div> -->

<div class="overlay" id="loading">
  <div class="sk-circle">
  <div class="sk-circle1 sk-child"></div>
  <div class="sk-circle2 sk-child"></div>
  <div class="sk-circle3 sk-child"></div>
  <div class="sk-circle4 sk-child"></div>
  <div class="sk-circle5 sk-child"></div>
  <div class="sk-circle6 sk-child"></div>
  <div class="sk-circle7 sk-child"></div>
  <div class="sk-circle8 sk-child"></div>
  <div class="sk-circle9 sk-child"></div>
  <div class="sk-circle10 sk-child"></div>
  <div class="sk-circle11 sk-child"></div>
  <div class="sk-circle12 sk-child"></div>
</div>
</div>

<div class="outer_container container">
  <div class="top_div" id="home">
    <div class="header_div container">
      <div class="col-sm-2">
      </div>
      <div class="col-sm-8 head_spans">
        <span class="menu_span" slideID="about_us">About | </span>
        <span class="menu_span" slideID="prop">Properties | </span>
        <span class="menu_span" slideID="amen">Amenities | </span>
        <span class="menu_span" slideID="location">Location | </span>
        <span class="menu_span" slideID="payment">Payment Terms | </span>
        <span class="menu_span" slideID="contact_us">Contact Us</span>
        <a class="remove_mobile" href="tel:16750"><span><img src="assets/img/phone.png"/>16750</span></a>
        <a class="remove_mobile" href="https://www.facebook.com/SaraiNewCairo" target="_blank">
          <span class="top_img"><img src="assets/img/fb_icon.png"/></span>
        </a>
        <a class="remove_mobile" href="https://www.instagram.com/sarai_newcairo/" target="_blank">
          <span class="top_img"><img src="assets/img/insta_icon.png"/></span>
        </a>
      </div>
      <div class="col-sm-2">
      </div>
    </div>
    <div class="top_div_main">
      <img src="assets/img/sarai_logo.png"/>
      <!-- <img src="assets/img/main_bg_3.jpg"/> -->
    </div>
<!--     <div class="top_div_text">
      <span class="top_div_text_span_2">The rhythm is yours.</span>
    </div> -->
    <div class="top_div_text_2 module" id="about_us">
      <p class="top_div_text_2_p">
        Sarai believes that diversity is the key to life. Diversity promotes curiosity, exploration and discovery - allowing people to pursue their dreams and explore new angles that propel them outwards.

Sarai employs a new concept of living that believes in personalizing every person’s living experience for their own luxury. Whether you seek a nurturing home that acts as a hive for your growing family or you’re after a custom-tailored home layout that guarantees you your very own private haven and oasis; Sarai has what you’re looking for.

Explore the unrivaled lifestyle of Sarai, where the rhythm of life is dictated by your choices and the pace is yours to set.
 
Located directly on the Cairo-Suez road, Sarai offers a perfect mix of urban to suburban exposure with a wide variety of spaces and units ranging from apartments to S-villas/ Town Houses among others set amidst a sprawling lush green landscape.
      </p>
      <span class="top_div_text_2_span_1">BENOY, the world’s master planning leaders</span>
      <p class="top_div_text_2_p top_div_text_2_p_2">
        With a global network in 60+ countries, Benoy is an established design brand, eminent for being the master planner of the Formula One race track. Signed by Benoy, Sarai’s tailored master plan represents your home’s unique elements and surroundings to reflect our appreciation of exceptionality. In addition to Benoy’s global expertise, our local experts have beautifully designed homes with bespoke interiors and warm textures that mirror Egyptian heritage and shared values – a true reflection of you and your family.
      </p>
    </div>
  </div>
  <div class="slider_div">
    <?php
    echo do_shortcode('[rev_slider alias="top_slider"]');
    ?>
  </div>
  <div class="ground_div container">
    <div class="col-sm-1 dummy_div">
    </div>
    <div class="col-sm-6 ground_div_text">
      <span class="ground_div_span_1">Introducing</span>
      <span class="ground_div_span_2">Sarai’s Ground</span>
      <span class="ground_div_span_3">Breaking Innovation</span>
      <span class="ground_div_span_4">View More</span>
      <div class="videoWrapper">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/py_8cPSKmL8?autoplay=0&rel=0&showinfo=0&controls=0" frameborder="0" allowfullscreen></iframe>
      </div>
      <p class="landing_ad_p">Own your dream family home in a one-of-a-kind S-villa at Sarai, New Cairo, starting from 260 sqm with 0% down payment and instalment plans up to 10 years. Don't let the opportunity go to waste, sign up today!</p>
    </div>
    <div class="col-sm-5">
    </div>
  </div>
  <div class="ground_div_2">
    <span class="ground_div_2_span_1">Introducing</span>
    <span class="ground_div_2_span_2">The S-Villa</span>
    <img class="ground_div_2_img" src="assets/img/s_villa_img.jpg"/>
    <p class="ground_div_2_p">Own your dream family home in a one-of-a-kind S-villa at Sarai, New Cairo, starting from 260 sqm with 0% down payment and instalment plans up to 10 years. Don't let the opportunity go to waste, sign up today!</p>
  </div>
  <div class="prop_div module" id="prop">
    <div class="prop_div_span_1_div">
      <span class="prop_div_span_1">Properties</span>
    </div>
    <div class="prop_div_span_2_div">
      <span class="prop_div_span_2 prop_div_span_2_1 apart_span active_1">Apartements</span>
      <span class=""> | </span>
      <span class="prop_div_span_2 prop_div_span_2_2 villa_span">S-Villa</span>
    </div>
    <div class="prop_slider container">
      <div id="owl-demo" class="owl-carousel owl-theme owl-demo pres_owl">
        <?php
          foreach ($perspectives_array as $key => $value) {
            
            ?>
            <div class="item row_1_item">
              <a class="foobox" href="<?php echo $value['image']; ?>" rel="gallery">
                <img class="" src="<?php echo $value['thumb']; ?>"/>
              </a>
            </div>
            <?php
          }
          ?>
      </div>
      <div id="owl-demo" class="owl-carousel owl-theme owl-demo floor_owl hidden_slider">
        <?php
          foreach ($floor_plans_array as $key => $value) {
            
            ?>
            <div class="item row_1_item">
              <a class="foobox" href="<?php echo $value['image']; ?>" rel="gallery2">
                <img class="" src="<?php echo $value['thumb']; ?>"/>
              </a>
            </div>
            <?php
          }
          ?>
      </div>
      <div id="owl-demo" class="owl-carousel owl-theme owl-demo pres_owl_2 hidden_slider">
        <?php
          foreach ($svilla_perspectives_array as $key => $value) {
            
            ?>
            <div class="item row_1_item">
              <a class="foobox" href="<?php echo $value['image']; ?>" rel="gallery3">
                <img class="" src="<?php echo $value['thumb']; ?>"/>
              </a>
            </div>
            <?php
          }
          ?>
      </div>
      <div id="owl-demo" class="owl-carousel owl-theme owl-demo floor_owl_2 hidden_slider">
        <?php
          foreach ($svilla_floor_plans_array as $key => $value) {
            
            ?>
            <div class="item row_1_item">
              <a class="foobox" href="<?php echo $value['image']; ?>" rel="gallery4">
                <img class="" src="<?php echo $value['thumb']; ?>"/>
              </a>
            </div>
            <?php
          }
          ?>
      </div>
    </div>
    <div class="prop_div_span_2_div">
      <span class="prop_div_span_3 prop_div_span_2_3 active_2">Perspectives</span>
      <span class="sep_span sep_hide"> | </span>
      <span class="prop_div_span_3 prop_div_span_2_4 sep_hide">floor Plans</span>
    </div>
    <div class="prop_bot_div">
      <span class="prop_bot_div_span">Sarai, the edge of aspiration.</span>
      <p class="prop_bot_div_p">
        Set amidst acres of lush landscapes and waterways, Sarai is where luxury, convenience and joy come together. It’s a world of possibilities, where couples settle, families grow, businesses flourish and children excel. Sarai is where you set your own pace – where the rhythm is yours
      </p>
    </div>
  </div>
  <div class="amen_div container module" id="amen">
    <div class="amen_div_span_1_div">
      <span class="amen_div_span_1">Services & Amenities</span>
    </div>
    <div id="owl-demo" class="owl-carousel owl-theme owl-demo_2">
          <?php
          foreach ($services_array as $key => $value) {
            
            ?>
            <div class="item row_1_item serv_item">
              <span><?php echo $value['title']; ?></span>
              <img src="<?php echo $value['banner']; ?>"/>
              <p>
                <?php echo $value['excerpt']; ?>
              </p>
              <div class="hidden_image_div">
                <span><?php echo $value['title']; ?></span>
                <img src="<?php echo $value['image']; ?>"/>
                <p><?php echo $value['content']; ?></p>
              </div>
            </div>
            <?php
          }
          ?>
      </div>
  </div>
  <div class="location_div module" id="location">
    <div class="location_div_span_1_div">
      <span class="location_div_span_1">Location</span>
    </div>
    <div class="location_div_span_2_div">
      <span class="location_div_span_2">Live a world away, minutes from all city corners</span>
    </div>
    <div class="location_div_p_div">
      <p class="location_div_p">Covering 5.5 million m2, Sarai is strategically located on the Cairo-Suez Road. We’ve researched and invested in the best location, bringing Sarai to the heart of Cairo’s most desirable future suburbs. Far from the city’s traffic, noise and pollution, it is 15 minutes away from the Ring Road, 10 from the AUC Campus, and 5 from the New Capital, as well as in close proximity to Suez road and New Cairo’s greatest attractions.</p>
    </div>
    <div class="maps_div">
      <div id="map1" class="maps">
      </div>
    </div>
  </div>
  <div class="pay_div module" id="payment">
    <span class="pay_div_span">Payment Terms</span>
    <p class="pay_div_p">In Sarai, we care about our clients and as we aim to offer them their dream houses, we also make sure to provide them with different payment terms that fit their abilities and requirements.</p>
    <div class="pay_div_images_div">
      <img src="assets/img/pay_1.png"/>
      <img src="assets/img/pay_2.png"/>
      <!-- <img src="assets/img/pay_3.jpg"/> -->
    </div>
  </div>
  <div class="contact_div module" id="contact_us">
    <div class="contact_div_span_1_div">
      <span class="contact_div_span_1">Contact Us</span>
    </div>
    <div class="contact_div_span_2_div">
      <span class="contact_div_span_2"><img src="assets/img/phone.png"/>16750</span>
    </div>
    <div class="contact_div_p_div">
      <p class="contact_div_p">We are always keen on giving you the best so choose the most convenient way to reach us</p>
    </div>
    <div class="contact_div_span_1_div">
      <span class="contact_div_span_2">Visit Us</span>
    </div>
    <div class="contact_div_span_3_1_div">
      <span class="contact_div_span_3">Sales Offices</span>
    </div>
    <div class="contact_div_span_3_div">
      <span class="contact_div_span_4">New Cairo:</span><span class="contact_div_span_5"> JW Marriott Hotel, Horus Hall.</span>
    </div>
    <div class="contact_div_span_3_div">
      <span class="contact_div_span_4">Cairo - Suez Rd.:</span><span class="contact_div_span_5"> Located on the extension of El Thawra St., in front of Cairo international airport. </span>
    </div>
  </div>
  <div class="reg_div module">
    <div class="reg_frame">
      <span class="reg_div_span">Register Here</span>
      <form class="reg_form" formID="1">
      <div class="form-group">
        <input inputID="1" type="text" class="form-control form_inputs form_name" placeholder="Name" required>
      </div>
      <div class="form-group">
        <input inputID="2" type="text" class="form-control form_inputs form_mobile" placeholder="Mobile" required>
      </div>
      <div class="form-group">
        <input inputID="3" type="email" class="form-control form_inputs form_email" placeholder="Email" required>
      </div>
      <div class="form-group">
        <input inputID="4" type="text" class="form-control form_inputs" placeholder="Country of Residence" required>
      </div>
      <!-- <div class="form-group">
        <input inputID="6" type="text" class="form-control form_inputs" placeholder="Property of Interest" required>
      </div> -->
      <div class="form-group">
            <select inputID="6" name="prop_of_interest" class="form-control form_select form_inputs prop_of_interest" required>
                <option value = "" style="color:white">Property of Interest</option>
                <option value = "Apartement 126 - 141 m2">Apartement 126 - 141 m2</option>
                <option value = "Apartement 164 - 182 m2">Apartement 164 - 182 m2</option>
                <option value = "S-Villa 260 - 295 m2">S-Villa 260 - 295 m2</option>
              </select>
          </div>

      <input inputID="5" type="hidden" value="<?php echo $new_url;?>" name="source" class="form_inputs">
      <button type="submit" class="btn btn-default submit_btn">Register</button>
    </form>
    </div>
  </div>
  <div class="footer_div footer container">
    <img class="menu_span top_arrow" slideID="home" src="assets/img/top_arrow.png"/>
    <div class="col-sm-3 pri_div">
      <a href="<?php echo get_permalink( 233 ); ?>">
        <span>Privacy Policy</span>
      </a>
    </div>
    <div class="col-sm-6">
      <span class="footer_div_span">All rights reserved. MNHD <?php echo date('Y')?></span>
      <div class="footer_icons">
        <a href="https://www.facebook.com/SaraiNewCairo" target="_blank">
          <img src="assets/img/fb_icon.png"/>
        </a>
        <a href="https://www.instagram.com/sarai_newcairo/" target="_blank">
          <img src="assets/img/insta_icon.png"/>
        </a>
      </div>
    </div>
    <div class="col-sm-3 footer_last">
      <span>A project by: </span>
      <span>Madinet Nasr for Housing & Development (MNHD)</span>
    </div>
  </div>
</div>


<button class="md-trigger md_trigger_sending" data-modal="modal-2"></button>

<button class="md-trigger md_trigger_image" data-modal="modal-4"></button>

<button class="md-trigger md_trigger_service" data-modal="modal-5"></button>

<div class="md-modal md-effect-2" id="modal-2">
  <div class="md-content">
    
  </div>
  <button class="md-close"></button>
</div>


<div class="md-modal md-effect-22" id="modal-4">
  <div class="md-content">
    
  </div>
  <button class="md-close">X</button>
</div>

<div class="md-modal md-effect-22" id="modal-5">
  <div class="md-content">
    
  </div>
  <button class="md-close">X</button>
</div>

<div class="md-overlay"></div>
<link rel="stylesheet" type="text/css" href="assets/modal/component.css" />


<input value="<?php echo site_url(); ?>/wp-content/themes/twentyfifteen/assets/img/sarai_marker.png" type="hidden" class="marker_url">
<input type="hidden" class="thank_you_url" value="<?php echo get_permalink( 75 ); ?>">

<?php get_footer(); ?>


</body>


<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/visible.js"></script>

<link href="assets/owl_carousel_assets/owl.carousel.css" rel="stylesheet" media="screen">
<link href="assets/owl_carousel_assets/owl.theme.css" rel="stylesheet" media="screen">
<link href="assets/owl_carousel_assets/owl.transitions.css" rel="stylesheet" media="screen">
<script type="text/javascript" src="assets/owl_carousel_assets/owl.carousel.js"></script>
<script type="text/javascript" src="assets/js/imagelightbox.js"></script>
<script src="assets/modal/modernizr.custom.js"></script>
<script src="assets/modal/classie.js"></script>
<script src="assets/modal/modalEffects.js"></script>

<script>

(function($) {

  /**
   * Copyright 2012, Digital Fusion
   * Licensed under the MIT license.
   * http://teamdf.com/jquery-plugins/license/
   *
   * @author Sam Sehnert
   * @desc A small plugin that checks whether elements are within
   *     the user visible viewport of a web browser.
   *     only accounts for vertical position, not horizontal.
   */

  $.fn.visible = function(partial) {
    
      var $t            = $(this),
          $w            = $(window),
          viewTop       = $w.scrollTop(),
          viewBottom    = viewTop + $w.height(),
          _top          = $t.offset().top,
          _bottom       = _top + $t.height(),
          compareTop    = partial === true ? _bottom : _top,
          compareBottom = partial === true ? _top : _bottom;
    
    return ((compareBottom <= viewBottom) && (compareTop >= viewTop));

  };
    
})(jQuery);

$( document ).ready(function() {
// $('.md_trigger_sending').trigger('click');
  var pres_owl = $(".pres_owl");
  pres_owl.owlCarousel({
     
     // items : 5,
      items : 4, //10 items above 1000px browser width
      itemsDesktop : [1300,4], //5 items between 1000px and 901px
      itemsDesktopSmall : [1130,3], // betweem 900px and 601px
      itemsTablet: [897,2], //2 items between 600 and 0
      itemsMobile : [608,1], // itemsMobile disabled - inherit from itemsTablet option
      navigation : true,
      autoPlay: 2000,
      rewindSpeed: 0
      // scrollPerPage : true
  });

  var pres_owl_2 = $(".pres_owl_2");
  pres_owl_2.owlCarousel({
     
     // items : 5,
      items : 4, //10 items above 1000px browser width
      itemsDesktop : [1300,4], //5 items between 1000px and 901px
      itemsDesktopSmall : [1130,3], // betweem 900px and 601px
      itemsTablet: [897,2], //2 items between 600 and 0
      itemsMobile : [608,1], // itemsMobile disabled - inherit from itemsTablet option
      navigation : true,
      autoPlay: 2000,
      rewindSpeed: 0
      // scrollPerPage : true
  });

  var floor_owl = $(".floor_owl");
  floor_owl.owlCarousel({
     
     // items : 5,
      items : 4, //10 items above 1000px browser width
      itemsDesktop : [1300,4], //5 items between 1000px and 901px
      itemsDesktopSmall : [1130,3], // betweem 900px and 601px
      itemsTablet: [897,2], //2 items between 600 and 0
      itemsMobile : [608,1], // itemsMobile disabled - inherit from itemsTablet option
      navigation : true,
      autoPlay: 2000,
      rewindSpeed: 0
      // scrollPerPage : true
  });

  var floor_owl_2 = $(".floor_owl_2");
  floor_owl_2.owlCarousel({
     
     // items : 5,
      items : 4, //10 items above 1000px browser width
      itemsDesktop : [1300,4], //5 items between 1000px and 901px
      itemsDesktopSmall : [1130,3], // betweem 900px and 601px
      itemsTablet: [897,2], //2 items between 600 and 0
      itemsMobile : [608,1], // itemsMobile disabled - inherit from itemsTablet option
      navigation : true,
      autoPlay: 2000,
      rewindSpeed: 0
      // scrollPerPage : true
  });

  var owl = $(".owl-demo_2");
  owl.owlCarousel({
     
     // items : 5,
      items : 3, //10 items above 1000px browser width
      itemsDesktop : [1400,3], //5 items between 1000px and 901px
      itemsDesktopSmall : [1130,2], // betweem 900px and 601px
      itemsTablet: [797,1], //2 items between 600 and 0
      itemsMobile : [608,1], // itemsMobile disabled - inherit from itemsTablet option
      navigation : true,
      autoPlay: 2000,
      // rewindNav:false
      rewindSpeed: 0
      // autoPlay: 2000
      // scrollPerPage : true
  });




$('.serv_item').on('click',function (e) {

    e.preventDefault();
    e.stopPropagation();

    $('#modal-5 .md-content').html($(this).parent().find('.hidden_image_div').html());

    $('.md-close').trigger('click');
    $('.md_trigger_service').trigger('click');

  });

$('.floor_img').on('click',function (e) {

    e.preventDefault();
    e.stopPropagation();

    $('#modal-4 .md-content').html($(this).parent().find('.hidden_image_div').html());

    $('.md-close').trigger('click');
    $('.md_trigger_image').trigger('click');

  });

  $('.prop_div_span_2_1').on('click',function (e) {

    e.preventDefault();
    e.stopPropagation();

    $('.active_1').removeClass('active_1');
    $(this).addClass('active_1');

    $('.active_2').removeClass('active_2');
    $('.prop_div_span_2_3').addClass('active_2');

    $('.pres_owl').removeClass('hidden_slider');
    $('.pres_owl_2').addClass('hidden_slider');
    $('.floor_owl').addClass('hidden_slider');
    $('.floor_owl_2').addClass('hidden_slider');

    $('.sep_span').addClass('sep_hide');
    $('.prop_div_span_2_4').addClass('sep_hide');

  });

  $('.prop_div_span_2_2').on('click',function (e) {

    e.preventDefault();
    e.stopPropagation();

    $('.active_1').removeClass('active_1');
    $(this).addClass('active_1');

    $('.active_2').removeClass('active_2');
    $('.prop_div_span_2_3').addClass('active_2');

    $('.pres_owl_2').removeClass('hidden_slider');
    $('.pres_owl').addClass('hidden_slider');
    $('.floor_owl').addClass('hidden_slider');
    $('.floor_owl_2').addClass('hidden_slider');

    $('.sep_span').removeClass('sep_hide');
    $('.prop_div_span_2_4').removeClass('sep_hide');
    

  });

  $('.prop_div_span_2_3').on('click',function (e) {

    e.preventDefault();
    e.stopPropagation();

    $('.active_2').removeClass('active_2');
    $(this).addClass('active_2');

    $('.floor_owl').addClass('hidden_slider');
    $('.floor_owl_2').addClass('hidden_slider');

    if($('.prop_div_span_2_1').hasClass('active_1'))
    {
      $('.pres_owl').removeClass('hidden_slider');
      $('.pres_owl_2').addClass('hidden_slider');
    }

    else
    {
      $('.pres_owl_2').removeClass('hidden_slider');
      $('.pres_owl').addClass('hidden_slider');
    }

  });

  $('.prop_div_span_2_4').on('click',function (e) {

    e.preventDefault();
    e.stopPropagation();

    $('.active_2').removeClass('active_2');
    $(this).addClass('active_2');

    $('.pres_owl').addClass('hidden_slider');
    $('.pres_owl_2').addClass('hidden_slider');

    if($('.prop_div_span_2_1').hasClass('active_1'))
    {
      $('.floor_owl').removeClass('hidden_slider');
      $('.floor_owl_2').addClass('hidden_slider');
    }

    else
    {
      $('.floor_owl_2').removeClass('hidden_slider');
      $('.floor_owl').addClass('hidden_slider');
    }

  });


  $('.menu_span').on('click',function (e) {

    e.preventDefault();
    e.stopPropagation();

    var slideID = $(this).attr('slideID');
    var post = $('#'+slideID);

    
    var new_height = parseInt($('.header_div').css('height'));

    var position = post.position().top - $(window).scrollTop();
    $('html, body').stop().animate({
        'scrollTop': post.offset().top - new_height
    }, 900, 'swing', function () {
        
    });

  });

  $('.landing_ad_x').on('click',function (e) {

    $('#landing_ad').fadeOut(250);

  });

  $('.ground_div_span_4').on('click',function (e) {

    $('.ground_div').fadeOut(250);
    $('.ground_div_2').fadeIn(500);

  });

  $( ".reg_form" ).submit(function( event ) {
    
    event.preventDefault();
    return false;

    var html = '<h3><img src="assets/img/spinner.gif"/>Registering...</h3>';

    $('#modal-2 .md-content').html(html);
    
    $('.md-close').trigger('click');
    $('.md_trigger_sending').trigger('click');


    var formID = $(this).attr('formID');
    var inputs = $(this).find('.form_inputs');

    $.each( inputs, function( key, value ) {
      
      inputID = $(value).attr('inputID');
      var input_old = $(value).val();
      $('#input_' + formID + '_' + inputID).val(input_old);


    });

    $('#gform_' + formID).submit();

    var name = $('.form_name').val();
    var mobile = $('.form_mobile').val();
    var email = $('.form_email').val();

    //http://159.203.168.233/crm/addLead2.php?name=xxxx&mobile=xxxxxx&email=xxxxx

  //   $.ajax({
  //   type: "POST",
  //   url: "http://159.203.168.233/crm/addLead3.php",
  //   data:{name:name, mobile:mobile, email:email},
  //   success: function(data){

  //    var url = $('.thank_you_url').val();
  //     window.location.href = url;

  //   }
  // });

  //   $.ajax({ 
  //   url: 'http://159.203.168.233/crm/addLead2.php',
  //   type: 'POST',
  //   data: {
  //       name:name,    
  //       email:email,
  //       mobile:mobile
  //   }, 
  //   success:function(data){

 
  //   }
  // }); 

        jQuery(document).bind('gform_post_render', function(){
   

    var url = $('.thank_you_url').val();
    window.location.href = url;

    $.each( inputs, function( key, value ) {
      
      $(value).val('');

    });

  });
  


     // $.post("http://159.203.168.233/crm/addLead2.php", { name:name, mobile:mobile, email:email },
     //   function(data) {
     //     alert("Data Loaded: " + data);
     //   });

  //   jQuery(document).bind('gform_post_render', function(){
   

  //   var url = $('.thank_you_url').val();
  //   window.location.href = url;

  //   $.each( inputs, function( key, value ) {
      
  //     $(value).val('');

  //   });

  // });

  });

  var win = $(window);
var allMods = $(".module");

// Already visible modules
allMods.each(function(i, el) {
  var el = $(el);
  if (el.visible(true)) {
    el.addClass("already-visible"); 
  } 
});

win.scroll(function(event) {
  
  allMods.each(function(i, el) {
    var el = $(el);
    if (el.visible(true)) {
      el.addClass("come-in"); 
    } 
  });

  var scroll = $(window).scrollTop();
    if(scroll == 0)
    {
      $('.header_div').removeClass('top_head_class');
    }
    else
    {
      $('.header_div').addClass('top_head_class');
    }
  
});

});

window.onload = function() {


$('.overlay').fadeOut(500);
$('.outer_container').fadeIn(750);
$('.owl-demo_2 .item p').css('width', $('.owl-demo_2 .item img').css('width'));

$('#landing_ad').css('height', $(document).height());


initMap();
// $('#map1').css('width', '99%');
// $('#map1').css('width', '100%');

}

$(window).resize(function () {

    $('.owl-demo_2 .item p').css('width', $('.owl-demo_2 .item img').css('width'));
    // $('.owl-demo_2 .item p').css('width', $('.demo_img').css('width'));
});

$(window).scroll(function (event) {
    
});

</script>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCe8ySOOlQeodi3WENjiXmopN24CvJEsto"></script>
