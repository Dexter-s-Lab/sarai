<?php

/*
 Template Name: Facebook Tab
 */

 ?>
<?php get_header(); ?>


<style type="text/css">

.outer_container
{
  display: block;
  width: 800px !important;
}

.top_div_main
{
/*  padding-top: 30px;
  padding-bottom: 30px;*/
}

.bottom_div
{
  width: 100%;
  /*height: 400px;*/
  background-image: url("assets/img/fb_tab/reg_bg.jpg");
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
}

.bottom_div_left, .bottom_div_right
{
  padding: 25px;
}

.reg_frame
{
  width: 100%;
  border: solid 1px #295941;
  padding: 15px;
  padding-top: 20px;
  padding-bottom: 20px;
  text-align: center;
}

.reg_form
{
  margin-top: 20px;
}

.reg_div_span
{
  font-family: 'Helvetica Neue'; 
  color: #295941;
  font-size: 25px;
}

.form-control
{
  width: 100%;
  text-align: center;
  padding: 16px;
  border-radius: 15px;
  background-color: #779483;
}

.submit_btn
{
  background-color: #0e3329;
  border: solid 2px #0e3329;
  color: #fff;
  border-radius: 10px;
  padding: 3px;
  padding-left: 35px;
  padding-right: 35px;
}

input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}


.submit_btn:hover, .submit_btn:focus
{
  background-color: #fff !important;
  border-color: #0e3329 !important;
  color: #0e3329 !important;
}

.form-group
{
  margin-bottom: 35px;
}

input::-webkit-input-placeholder {
color: #dadada !important;
/*font-size: 20px;*/
}
 
input:-moz-placeholder { /* Firefox 18- */
color: #dadada !important;  
/*font-size: 20px;*/
}
 
input::-moz-placeholder {  /* Firefox 19+ */
color: #dadada !important;  
/*font-size: 20px;*/
}
 
input:-ms-input-placeholder {  
color: #dadada !important;  
/*font-size: 20px;*/
}

.bottom_div_right_1 span, .bottom_div_right_2 span
{
  font-family: 'Roboto-Regular';
  color: #0d4e31;
  font-weight: bold;
  font-size: 18px;
}

.bottom_div_right_1 p, .bottom_div_right_2 p
{
  font-family: 'Roboto-Regular';
  color: #444444;
  font-size: 14px;
  text-align: left;
  margin-top: 10px;
}

.bottom_div_right_1 p
{
  margin-bottom: 30px;
}

#gform_1, .md-trigger
{
  display: none;
}

.md-close, .md-trigger, #gforms_confirmation_message
{
    display: none;
}

.md-close
{
  position: absolute;
  top: -10px;
  left: -10px;
  color: #333333;
  border: solid 1px #333333;
  background-color: #fff;
}

.md-close:hover
{
  color: #fff;
  background-color: #333333;
  border-color: #333333;
}

.md-content > div
{
  padding-bottom: 10px !important;
}

.md-content
{
  background-color: #fff !important;
}
.md-content h3
{
  color: #123a24 !important;
}

.md-content img
{
  width: 50px;
  margin-right: 12px;
}

</style>
<title><?php echo $page_title; ?></title>
<body>

<div class="outer_container container">
  <div class="top_div" id="home">
    <div class="top_div_main">
      <?php
    echo do_shortcode('[rev_slider alias="facebook_tab"]');
    ?>
    </div>
  </div>
  <div class="bottom_div container">
    <div class="bottom_div_left col-sm-6">
      <div class="reg_frame">
        <span class="reg_div_span">Register Here</span>
        <form class="reg_form" formID="1">
        <div class="form-group">
          <input inputID="1" type="text" class="form-control form_inputs" placeholder="Name" required>
        </div>
        <div class="form-group">
          <input inputID="2" type="text" class="form-control form_inputs" placeholder="Mobile" required>
        </div>
        <div class="form-group">
          <input inputID="3" type="email" class="form-control form_inputs" placeholder="Email" required>
        </div>
        <div class="form-group">
          <input inputID="4" type="text" class="form-control form_inputs" placeholder="Country of Residence" required>
        </div>

        <input inputID="5" type="hidden" value="facebook tab" name="source" class="form_inputs">
        <button type="submit" class="btn btn-default submit_btn">Register</button>
      </form>
    </div>
    </div>
    <div class="bottom_div_right col-sm-6">
      <div class="bottom_div_right_1">
        <span>Sarai, the edge of aspiration.</span>
        <p>
          Set amidst acres of lush landscapes and waterways, Sarai is where luxury, convenience and joy come together. It’s a world of possibilities, where couples settle, families grow, businesses flourish and children excel. Sarai is where you set your own pace – where the rhythm is yours
        </p>
      </div>
      <div class="bottom_div_right_2">
        <span>A project by:</span>
        <p>
          Madinet Nasr for Housing & Development (MNHD)
        </p>
      </div>
    </div>
  </div>
</div>


<?php get_footer(); ?>

<?php
gravity_form(1, $display_title=true, $display_description=false, $display_inactive=false, $field_values=null, $ajax=true, 1);
?>

<!-- <input type="hidden" class="thank_you_url" value="<?php echo get_permalink( 82 ); ?>"> -->
<input type="hidden" class="thank_you_url" value="<?php echo get_permalink( 150 ); ?>">

<button class="md-trigger md_trigger_sending" data-modal="modal-2"></button>
<button class="md-trigger md_trigger_success" data-modal="modal-3"></button>

<div class="md-modal md-effect-2" id="modal-2">
  <div class="md-content">
    <h3><img src="assets/img/spinner.gif"/>Registering...</h3>
  </div>
  <button class="md-close"></button>
</div>


<div class="md-modal md-effect-22" id="modal-3">
  <div class="md-content">
    <h3>Registeration Successful!</h3>
  </div>
  <button class="md-close"></button>
</div>

<div class="md-overlay"></div>
<link rel="stylesheet" type="text/css" href="assets/modal/component.css" />
<script src="assets/modal/modernizr.custom.js"></script>
<script src="assets/modal/classie.js"></script>
<script src="assets/modal/modalEffects.js"></script>

</body>

<script>

  $( ".reg_form" ).submit(function( event ) {
    
    event.preventDefault();

    $('.md-close').trigger('click');
    $('.md_trigger_sending').trigger('click');


    var formID = $(this).attr('formID');
    var inputs = $(this).find('.form_inputs');

    $.each( inputs, function( key, value ) {
      
      inputID = $(value).attr('inputID');
      var input_old = $(value).val();
      $('#input_' + formID + '_' + inputID).val(input_old);


    });

    $('#gform_' + formID).submit();
    
    jQuery(document).bind('gform_post_render', function(){
   
     var url = $('.thank_you_url').val();
      window.location.href = url;

    $.each( inputs, function( key, value ) {
      
      $(value).val('');

    });

  });

  });

</script>