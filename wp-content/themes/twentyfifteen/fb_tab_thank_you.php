<?php

/*
 Template Name: Facebook Tab Thank You
 */

 ?>
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0">

<base href="<?php bloginfo('template_directory');?>/twentyfifteen"/>

<!-- <script type="text/javascript" src="assets/js/jquery-2.1.4.min.js"></script> -->
<script type="text/javascript" src="assets/js/jquery-2.1.3.min.js"></script>

<link href="assets/css/bootstrap.min.css" rel="stylesheet" media="screen">
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<!-- <script type="text/javascript" src="assets/fancybox-videos_2/lib/jquery-1.10.1.min.js"></script> --> 

<link href="assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" media="screen">

<script type="text/javascript" src="assets/js/bootstrap-submenu.min.js"></script>
<script type="text/javascript" src="assets/js/docs.js"></script>

<script type="text/javascript" src="assets/js/visible.js"></script>

<link href="assets/owl_carousel_assets/owl.carousel.css" rel="stylesheet" media="screen">
<link href="assets/owl_carousel_assets/owl.theme.css" rel="stylesheet" media="screen">
<link href="assets/owl_carousel_assets/owl.transitions.css" rel="stylesheet" media="screen">
<script type="text/javascript" src="assets/owl_carousel_assets/owl.carousel.js"></script>

<!-- <link href="assets/css/bootstrap-submenu.min.css" rel="stylesheet" media="screen"> -->

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '1162650763829848', {
em: 'insert_email_variable,'
});
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1162650763829848&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->

<?php wp_head() ?>
</head>


<style type="text/css">

.outer_container
{
  display: block;
  width: 800px !important;
}

.top_div_main
{
/*  padding-top: 30px;
  padding-bottom: 30px;*/
}

.bottom_div
{
  width: 100%;
  /*height: 400px;*/
  background-image: url("assets/img/fb_tab/reg_bg.jpg");
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
}

.bottom_div_left, .bottom_div_right
{
  padding: 25px;
}

.reg_frame
{
  width: 100%;
  border: solid 1px #295941;
  padding: 15px;
  padding-top: 20px;
  padding-bottom: 20px;
  text-align: center;
}

.reg_form
{
  margin-top: 20px;
}

.reg_div_span
{
  font-family: 'Helvetica Neue'; 
  color: #295941;
  font-size: 25px;
}

.form-control
{
  width: 100%;
  text-align: center;
  padding: 16px;
  border-radius: 15px;
  background-color: #779483;
}

.submit_btn
{
  background-color: #0e3329;
  border: solid 2px #0e3329;
  color: #fff;
  border-radius: 10px;
  padding: 3px;
  padding-left: 35px;
  padding-right: 35px;
}

input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}


.submit_btn:hover, .submit_btn:focus
{
  background-color: #fff !important;
  border-color: #0e3329 !important;
  color: #0e3329 !important;
}

.form-group
{
  margin-bottom: 35px;
}

input::-webkit-input-placeholder {
color: #dadada !important;
/*font-size: 20px;*/
}
 
input:-moz-placeholder { /* Firefox 18- */
color: #dadada !important;  
/*font-size: 20px;*/
}
 
input::-moz-placeholder {  /* Firefox 19+ */
color: #dadada !important;  
/*font-size: 20px;*/
}
 
input:-ms-input-placeholder {  
color: #dadada !important;  
/*font-size: 20px;*/
}

.bottom_div_right_1 span, .bottom_div_right_2 span
{
  font-family: 'Roboto-Regular';
  color: #0d4e31;
  font-weight: bold;
  font-size: 18px;
}

.bottom_div_right_1 p, .bottom_div_right_2 p
{
  font-family: 'Roboto-Regular';
  color: #444444;
  font-size: 14px;
  text-align: left;
  margin-top: 10px;
}

.bottom_div_right_1 p
{
  margin-bottom: 30px;
}

#gform_1, .md-trigger
{
  display: none;
}

.md-close, .md-trigger, #gforms_confirmation_message
{
    display: none;
}

.md-close
{
  position: absolute;
  top: -10px;
  left: -10px;
  color: #333333;
  border: solid 1px #333333;
  background-color: #fff;
}

.md-close:hover
{
  color: #fff;
  background-color: #333333;
  border-color: #333333;
}

.md-content > div
{
  padding-bottom: 10px !important;
}

.md-content
{
  background-color: #fff !important;
}
.md-content h3
{
  color: #123a24 !important;
}

.md-content img
{
  width: 50px;
  margin-right: 12px;
}

.top_div_text_2_span_1
{
  font-family: 'Roboto-Regular';
  color: #123a24;
  font-size: 20px;
  display: block;
  letter-spacing: 6px;
}

</style>
<title><?php echo $page_title; ?></title>
<body>

  <!-- Google Code for SARAI Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 869534557;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "_ELECN62umsQ3ZbQngM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/869534557/?label=_ELECN62umsQ3ZbQngM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<div class="outer_container container">
  <div class="top_div" id="home">
    <div class="top_div_main">
      <?php
    echo do_shortcode('[rev_slider alias="facebook_tab"]');
    ?>
    </div>
  </div>
  <div class="bottom_div container">
    <div class="bottom_div_left col-sm-12">
      <span class="top_div_text_2_span_1">Thank you for your registeration. Our sales team will get in touch with you soon.</span>
    </div>
  </div>
</div>


<?php get_footer(); ?>

</body>
