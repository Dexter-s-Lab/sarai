<?php

/*
 Template Name: Privacy Policy
 */

 ?>
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0">

<base href="<?php bloginfo('template_directory');?>/twentyfifteen"/>

<!-- <script type="text/javascript" src="assets/js/jquery-2.1.4.min.js"></script> -->
<script type="text/javascript" src="assets/js/jquery-2.1.3.min.js"></script>

<link href="assets/css/bootstrap.min.css" rel="stylesheet" media="screen">
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<!-- <script type="text/javascript" src="assets/fancybox-videos_2/lib/jquery-1.10.1.min.js"></script> --> 

<link href="assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" media="screen">

<script type="text/javascript" src="assets/js/bootstrap-submenu.min.js"></script>
<script type="text/javascript" src="assets/js/docs.js"></script>

<script type="text/javascript" src="assets/js/visible.js"></script>

<link href="assets/owl_carousel_assets/owl.carousel.css" rel="stylesheet" media="screen">
<link href="assets/owl_carousel_assets/owl.theme.css" rel="stylesheet" media="screen">
<link href="assets/owl_carousel_assets/owl.transitions.css" rel="stylesheet" media="screen">
<script type="text/javascript" src="assets/owl_carousel_assets/owl.carousel.js"></script>


<?php wp_head() ?>
</head>

<?php

global $post;
$id =  get_the_ID();
$text = get_field('text', $id);

?>


<link href="style_home.css" rel="stylesheet" media="screen">

<style type="text/css">

.outer_container
{
  display: block;
}

.top_div_main
{
  padding-top: 30px;
  padding-bottom: 30px;
}


.top_div_text_2_span_1 
{
  width: 80%;
  margin-left: auto;
  margin-right: auto;
  text-transform: inherit;
  letter-spacing: 0;
}

.top_div_text_2 
{
  padding: 20px;
  text-align: left;
}

@media screen and (max-width: 500px) {

.top_div_text_2_span_1 
{
  font-size: 15px;
}

.top_div_text_2
{
  padding-top: 4%;
}

}

</style>
<title><?php echo $page_title; ?></title>
<body>


<img src="http://bcp.crwdcntrl.net/5/c=5995/b=35038892" width="1" height="1" class="ad_img"/>

<div class="outer_container container">
  <div class="top_div" id="home">
    <div class="top_div_main">
      <img src="assets/img/sarai_logo.png"/>
    </div>
    <div class="top_div_text_2 module" id="about_us">
      <?php
      echo $text;
      ?>
    </div>
  </div>
  <div class="footer_div footer container">
    <div class="col-sm-3 pri_div">
      <a href="<?php echo get_permalink( 233 ); ?>">
        <span>Privacy Policy</span>
      </a>
    </div>
    <div class="col-sm-6">
      <span class="footer_div_span">All rights reserved. MNHD <?php echo date('Y')?></span>
      <div class="footer_icons">
        <a href="https://www.facebook.com/SaraiNewCairo" target="_blank">
          <img src="assets/img/fb_icon.png"/>
        </a>
        <a href="https://www.instagram.com/sarai_newcairo/" target="_blank">
          <img src="assets/img/insta_icon.png"/>
        </a>
      </div>
    </div>
    <div class="col-sm-3 footer_last">
      <span>A project by: </span>
      <span>Madinet Nasr for Housing & Development (MNHD)</span>
    </div>
  </div>
</div>



<?php get_footer(); ?>


</body>
